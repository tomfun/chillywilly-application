import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export function rerenderAll() {
    ReactDOM.render(<App/>, document.getElementById('root'));
}

var tupayaPeremennayaVmestoState = "начальное состояние";
var anotherStateArray = [];

class App extends Component {
    render() {
        return (
            <div>
                <button onClick={() => {
                    tupayaPeremennayaVmestoState = Math.random();
                    anotherStateArray.push(tupayaPeremennayaVmestoState);
                    rerenderAll();
                }}>
                    {tupayaPeremennayaVmestoState}
                </button>
                anotherStateArray:
                <pre>
                    {JSON.stringify(anotherStateArray, null, 4)}
                </pre>
                {anotherStateArray.map(v => <li>{v}</li>)}
            </div>
        );
    }
}

export default App;
