import React, { Component } from 'react';

import './Dropdown.css';

class Dropdown extends Component {
    
    constructor(props) {
        super(props);
        this.state = {isOpened: false};
    }
    
    toggleState() {
        this.setState({isOpened: !this.state.isOpened})
    }
    
    render() {
        console.log('isOpened:', this.state.isOpened);
        let dropdownText;
        if (this.state.isOpened) {
            dropdownText = <div className="data-text">content</div>
        }
        return (
            <div className="Dropdown" onClick={this.toggleState.bind(this)}>
                Menu
                {dropdownText}
            </div>);
    }
}

export default Dropdown;
